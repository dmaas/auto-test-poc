#!/usr/bin/env python
import os
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.igext as IG
import geni.rspec.emulab.pnext as PN
import geni.urn as URN


tourDescription = """
### Automated Testing POC

This profile allocates resources in a controlled RF environment for a test
automation proof-of-concept with open source 5G CN (Open5GS) and RAN (OAI
gNodeB) software, as well as a 5G COTS UE module (Quectel RM520N). Hardware
resources include: (1) two X310 SDRs, each with two UBX-160 daughter cards
installed, and paired with server-class compute; and (2) a single NUC with the
5G module connected via USB3. The TX/RX and RX2 ports for each daughter card are
connected to the UE via paths in the attenuator matrix and the attentuation for
these paths can be adjusted in real time."""

tourInstructions = """

Note: this profile includes startup scripts that download, install, and
configure the required software stacks. After the experiment becomes ready, wait
until the "Startup" column on the "List View" tab indicates that the startup
scripts have finished on all of the nodes before proceeding.

#### Instructions (WIP)

After all startup scripts have finished, the Open5GS CN will be running as a set
of services on the `cn` node. All matrix paths have a minumum of 30 dB
attenuatation. They will start with 0 dB additional attenuation. You can use the
`atten` utility to add additional attenuation (and remove it) for each path
individually, or `update-attens` to adjust multiple paths at once. Type
`/local/repository/bin/atten -h` or `/local/repository/bin/update-attens -h` for
help.

The following steps, e.g., will be automated:

1. Setup UE (start connection manager: `sudo quectel-CM -s internet -4`; set airplane mode by, e.g., sending `AT+CFUN=4`)
2. Start OAI gNodeB (`sudo /var/tmp/oairan/cmake_targets/ran_build/build/nr-softmodem -O /var/tmp/etc/oai/gnb.sa.b77.x310.conf -E --sa --tune-offset=40000000` on one of gnb{1,2})
3. Attach UE (by, e.g., sending `AT+CFUN=1` to bring it out of airplane mode)
4. Test throughput
5. Detach UE (by toggling airplane mode again)
6. Adjust attenuation
7. Repeat...

Second gNodeB SDR/compute just there for testing the matrix connections atm, but
could be used to generate noise.

"""

BIN_PATH = "/local/repository/bin"
ETC_PATH = "/local/repository/etc"
LOWLAT_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:U18LL-SRSLTE"
UBUNTU_IMG = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
COTS_UE_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:cots-focal-image"
COMP_MANAGER_ID = "urn:publicid:IDN+emulab.net+authority+cm"
DEFAULT_NR_RAN_HASH = "61312ca6e29addc41b46a9f8cb1a28ee73e42d84" # 2022.wk32
OAI_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-oai.sh")
OPEN5GS_DEPLOY_SCRIPT = os.path.join(BIN_PATH, "deploy-open5gs.sh")
TUNE_CPU = os.path.join(BIN_PATH, "tune-cpu.sh")


pc = portal.Context()
node_type = [
    ("d740",
     "Emulab, d740"),
    ("d430",
     "Emulab, d430")
]

pc.defineParameter("cn_node_type",
                   "Type of compute node for Open5GS CN",
                   portal.ParameterType.STRING,
                   node_type[0],
                   node_type)

pc.defineParameter(
    name="oai_ran_commit_hash",
    description="Commit hash for OAI RAN",
    typ=portal.ParameterType.STRING,
    defaultValue="",
    advanced=True
)

params = pc.bindParameters()
pc.verifyParameters()
request = pc.makeRequestRSpec()

cn = request.RawPC("cn")
cn.hardware_type = params.cn_node_type
cn.disk_image = UBUNTU_IMG
cn.addService(rspec.Execute(shell="bash", command=OPEN5GS_DEPLOY_SCRIPT))
cn_ng_if = cn.addInterface("cn_ng_if")
cn_ng_if.addAddress(rspec.IPv4Address("192.168.1.1", "255.255.255.0"))

ue = request.RawPC("ue")
ue.component_manager_id = COMP_MANAGER_ID
ue.component_id = "nuc22"
ue.disk_image = COTS_UE_IMG
ue.Desire("rf-controlled", 1)
ue_gnb1_sdr_if = ue.addInterface("ue-gnb1-sdr-if")
ue_gnb2_sdr_if = ue.addInterface("ue-gnb2-sdr-if")

if params.oai_ran_commit_hash:
    oai_ran_hash = params.oai_ran_commit_hash
else:
    oai_ran_hash = DEFAULT_NR_RAN_HASH

gnb1 = request.RawPC("gnb1")
gnb1.component_manager_id = COMP_MANAGER_ID
gnb1.hardware_type = "d740"

gnb1.disk_image = LOWLAT_IMG
gnb1_ng_if = gnb1.addInterface("gnb1-ng-if")
gnb1_ng_if.addAddress(rspec.IPv4Address("192.168.1.2", "255.255.255.0"))

gnb1_usrp_if = gnb1.addInterface("gnb1-usrp-if")
gnb1_usrp_if.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))

cmd = '{} "{}" {}'.format(OAI_DEPLOY_SCRIPT, oai_ran_hash, "nodeb")
gnb1.addService(rspec.Execute(shell="bash", command=cmd))
gnb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
gnb1.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))

gnb1_sdr = request.RawPC("gnb1-sdr")
gnb1_sdr.component_manager_id = COMP_MANAGER_ID
gnb1_sdr.component_id = "oai-wb-c1"
gnb1_sdr.Desire("rf-controlled", 1)
gnb1_sdr_if = gnb1_sdr.addInterface("gnb1-sdr-if")
gnb1_sdr_ue_if = gnb1_sdr.addInterface("gnb1-sdr-ue-if")

gnb1_sdr_link = request.Link("gnb1-sdr-link")
gnb1_sdr_link.bandwidth = 10*1000*1000
gnb1_sdr_link.addInterface(gnb1_usrp_if)
gnb1_sdr_link.addInterface(gnb1_sdr_if)

gnb2 = request.RawPC("gnb2")
gnb2.component_manager_id = COMP_MANAGER_ID
gnb2.hardware_type = "d740"

gnb2.disk_image = LOWLAT_IMG
gnb2_ng_if = gnb2.addInterface("gnb2-ng-if")
gnb2_ng_if.addAddress(rspec.IPv4Address("192.168.1.3", "255.255.255.0"))
gnb2_ue_rf = gnb2.addInterface("gnb2-ue-rf")

gnb2_usrp_if = gnb2.addInterface("gnb2-usrp-if")
gnb2_usrp_if.addAddress(rspec.IPv4Address("192.168.40.1", "255.255.255.0"))

cmd = '{} "{}" {}'.format(OAI_DEPLOY_SCRIPT, oai_ran_hash, "nodeb")
gnb2.addService(rspec.Execute(shell="bash", command=cmd))
gnb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-cpu.sh"))
gnb2.addService(rspec.Execute(shell="bash", command="/local/repository/bin/tune-sdr-iface.sh"))

gnb2_sdr = request.RawPC("gnb2-sdr")
gnb2_sdr.component_manager_id = COMP_MANAGER_ID
gnb2_sdr.component_id = "oai-wb-c2"
gnb2_sdr.Desire("rf-controlled", 1)
gnb2_sdr_if = gnb2_sdr.addInterface("gnb2-sdr-if")
gnb2_sdr_ue_if = gnb2_sdr.addInterface("gnb2-sdr-ue-if")

gnb2_sdr_link = request.Link("gnb2-sdr-link")
gnb2_sdr_link.bandwidth = 10*1000*1000
gnb2_sdr_link.addInterface(gnb2_usrp_if)
gnb2_sdr_link.addInterface(gnb2_sdr_if)

# Create ng links between eNodeBs and CN
link = request.LAN("lan")
link.addInterface(cn_ng_if)
link.addInterface(gnb1_ng_if)
link.addInterface(gnb2_ng_if)
link.link_multiplexing = True
link.vlan_tagging = True
link.best_effort = True

# Create RF links between the UE and eNodeBs
rflink1 = request.RFLink("rflink1")
rflink1.addInterface(gnb1_sdr_ue_if)
rflink1.addInterface(ue_gnb1_sdr_if)

rflink2 = request.RFLink("rflink2")
rflink2.addInterface(gnb2_sdr_ue_if)
rflink2.addInterface(ue_gnb2_sdr_if)

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

pc.printRequestRSpec(request)
